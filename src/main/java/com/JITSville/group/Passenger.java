package com.JITSville.group;

public abstract class Passenger {
	private final double rateFactor = 0.5;
	private boolean newsPaper;
	
	public abstract double calculateFare();
	
	public boolean hasNewsPaper() {
		return newsPaper;
	}
	
	public void setNewsPaper(boolean newsPaper) {
		this.newsPaper = newsPaper;
	}
	
	public double getRateFactor() {
		return rateFactor;
	}
	
	@Override
	public String toString() {
		return "Passenger [rateFactor=" + rateFactor + ", newsPaper=" + newsPaper + "]";
	}
}

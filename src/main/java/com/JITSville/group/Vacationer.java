package com.JITSville.group;

public class Vacationer extends Passenger implements Meals {
	private int noofMiles;
	
	public Vacationer(int noofMiles, boolean newsPaper) {
		super();
		this.noofMiles = noofMiles;
		if (noofMiles < 5 || noofMiles > 4000) {
			throw new IllegalArgumentException();
		}
		super.setNewsPaper(newsPaper);
	}
	
	

	public void setNoofMiles(int noofMiles) {
		this.noofMiles = noofMiles;
	}



	@Override
	public double calculateFare() {
		return getRateFactor() * noofMiles;
	}

	public int getNoofMiles() {
		return noofMiles;
	}
	
	public int countMeals() {
		return getNoofMiles() / 100 + 1;
	}
}

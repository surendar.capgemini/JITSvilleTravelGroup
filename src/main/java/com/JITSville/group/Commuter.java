package com.JITSville.group;

public class Commuter extends Passenger {
	private int noofStops;
	private boolean frequencyCard;
	
	public Commuter(int noofStops, boolean newsPaper, boolean frequencyCard) {
		super();
		this.noofStops = noofStops;
		super.setNewsPaper(newsPaper);
		this.frequencyCard = frequencyCard;
	}
	
	public boolean hasFrequencyCard() {
		return frequencyCard;
	}

	@Override
	public double calculateFare() {
		if(hasFrequencyCard())
			return getRateFactor() * noofStops * 0.9;
		else
		return getRateFactor() * noofStops;
	}
}

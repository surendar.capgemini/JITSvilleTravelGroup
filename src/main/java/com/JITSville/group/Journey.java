package com.JITSville.group;

import java.util.ArrayList;
import java.util.List;

public class Journey {

	private static List<Passenger> passengers;
	private static int newsPapersCount = 0;
	private static int mealsCount = 0;
	private static double totalCost = 0;

	public static int countNewsPapers(List<Passenger> passengers) {
		for (Passenger passenger : passengers) {
			if (passenger.hasNewsPaper()) {
				newsPapersCount++;
			}
		}
		return newsPapersCount;
	}

	public static int countMeals(List<Passenger> passengers) {
		for (Passenger passenger : passengers) {
			if (passenger instanceof Vacationer) {
				mealsCount += ((Vacationer) passenger).countMeals();
			}
		}
		return mealsCount;
	}

	public static void main(String[] args) {
		passengers = new ArrayList<Passenger>();
		Passenger passenger1 = new Commuter(3, true, false);
		Passenger passenger2 = new Commuter(5, true, false);
		Passenger passenger3 = new Commuter(4, false, true);

		Passenger passenger4 = new Vacationer(90, true);
		Passenger passenger5 = new Vacationer(199, false);

		passengers.add(passenger1);
		passengers.add(passenger2);
		passengers.add(passenger3);
		passengers.add(passenger4);
		passengers.add(passenger5);

		for (Passenger p : passengers) {
			totalCost = totalCost + p.calculateFare();
		}

		System.out.println("Total Cost: " + totalCost);
		System.out.println("Meals Count: " + countMeals(passengers));
		System.out.println("NewsPapers Count: " + countNewsPapers(passengers));
	}
}

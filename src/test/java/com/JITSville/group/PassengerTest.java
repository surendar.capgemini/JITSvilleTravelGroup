package com.JITSville.group;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

public class PassengerTest {
	
	private static List<Passenger> passengers;
	
	@BeforeClass
	public static void setUp() {
		passengers = new ArrayList<Passenger>();
		passengers.add(new Commuter(6, true, true));
		passengers.add(new Commuter(3, false, false));
		passengers.add(new Vacationer(90, false));
		passengers.add(new Vacationer(199, true));
	}
	
	@Test
	public void testCommuterCalculateFareWithCard() {
		assertEquals(2.7, passengers.get(0).calculateFare(), .001);
	}
	
	@Test
	public void testCommuterCalculateFareWithoutCard() {
		assertEquals(1.5, passengers.get(1).calculateFare(), .001);
	}
	
	@Test
	public void testVacationerCalculateFare() {
		assertEquals(45.0, passengers.get(2).calculateFare(), .001);
	}
	
	@Test
	public void testNewsPapersCount() {
		assertEquals(2, Journey.countNewsPapers(passengers));
	}
	
	@Test
	public void testMealsCount() {
		assertEquals(3, Journey.countMeals(passengers));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testIllegalArgumentExceptionlst5() {
	    int a = 3;
	    Vacationer actual = new Vacationer(a, true);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testIllegalArgumentExceptionget4000() {
	    int a = 4500;
	    Vacationer actual = new Vacationer(a, true);
	}
}
